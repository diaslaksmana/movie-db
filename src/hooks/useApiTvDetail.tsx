import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';


export const useApiTvDetail = () => {
    const params = useParams();
    const[error, setError]= useState(null);
    const[isLoaded, setIsLoaded]=useState(false);
    const[cast, setCast]=useState<any>();
    const[movie, setMovie]=useState<any>();
    const[trailer, setTrailer]=useState<any>();
    const[similar, setSimilar]=useState<any>();
    const[genresbyId, setGenresbyId]=useState<any>();
    const[review, setReview]=useState<any>();
    const[season, setSeason]=useState<any>();
    const[seasonNum, setSeasonNum]=useState(1);
    const[episode, setEpisode]=useState<any>();
    const[showTrailer, setShowTrailer] = useState(false);
    
    //API Detail Movie
    useEffect (() =>{
            Promise.all([
                fetch(`http://api.themoviedb.org/3/tv/${params.id}?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US`),
                fetch(`https://api.themoviedb.org/3/tv/${params.id}/credits?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US`),
                fetch(`https://api.themoviedb.org/3/tv/${params.id}/videos?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US`),
                fetch(`https://api.themoviedb.org/3/tv/${params.id}/similar?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US&page=1`),
                fetch(`https://api.themoviedb.org/3/tv/${params.id}/reviews?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US&page=1`),
                fetch(`https://api.themoviedb.org/3/tv/${params.id}/season/${seasonNum}?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US`),
                fetch(`https://api.themoviedb.org/3/tv/${params.id}/season/${params.id}/episode/${params.epsNum}?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US`)
              ])
                .then(resp => Promise.all( resp.map(r => r.json()) ))
                .then(
                    ([data, cast, trailer,similar,review,season,episode ])=> {
                        setIsLoaded(true);
                        //console.log(data)
                        setMovie(data);   
                        setGenresbyId(data.genres);
                        //console.log(cast)
                        setCast(cast.cast); 
                        //console.log(trailer)
                        setTrailer(trailer.results); 
                        setSimilar(similar.results); 
                        setReview(review.results); 
                        console.log(season)
                        setSeason(season);
                        //setEpisode(episode?.results[0]);\
                        setEpisode(season);
                       
                        console.log(episode)
                    },
                    (error)=>{
                        setIsLoaded(true);
                        setError(error);
                    }
                )
               
                // window.scrollTo({
                //     top: 0,
                //     left: 0,
                //     behavior: "smooth"
                //   });
    }, [seasonNum,params.id,params.epsNum]);
 
    
    return {error, isLoaded,cast,movie,trailer,similar,genresbyId,review,season,seasonNum ,setSeasonNum ,episode,showTrailer, setShowTrailer}
}
