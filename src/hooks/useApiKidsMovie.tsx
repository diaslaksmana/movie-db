import React, { useEffect, useState } from 'react'
import apiSettings from '../service/Api';

type Props = {}

export const useApiKidsMovie = () => {
    const [error, setError] = useState<any>();
    const [isLoaded, setIsLoaded] = useState<boolean>(false);
    const [moviesList, setMoviesList] = useState<any>();
    const[genres, setGenres]=useState<any>();    
    const [pages, setPages] = useState<any>();
    useEffect(() => {
       Promise.all([
                fetch (apiSettings.fetchKidsMovie),
                fetch(apiSettings.fetchGenrelist)
              ])
     

            
            .then(resp => Promise.all( resp.map(r => r.json()) ))
            .then(
                ([data, genreData])=> {
                    setIsLoaded(true);
                    console.log(data)   
                    setMoviesList(data.results);   
                    setPages(data);  
                    console.log(genreData)
                    const genres=genreData.genres?.reduce((genres:any,gen:any) => {
                        const {id,name} = gen
                        genres[id] =name
                        return genres
                    },[])
                    setGenres(genres);                                            
                },
                (error)=>{
                    setIsLoaded(true);
                    setError(error);
                }
            )
        }, []);

      return { isLoaded, moviesList, genres, error,pages };
}