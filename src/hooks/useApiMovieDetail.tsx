import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';


export const useApiMovieDetail  = () => { 
  const moviesId = useParams();
  const[error, setError]= useState<any>();
  const[isLoaded, setIsLoaded]=useState<boolean>(false);
  const[cast, setCast]=useState<any>();
  const[movie, setMovie]=useState<any>();
  const[trailer, setTrailer]=useState<any>();
  const[similar, setSimilar]=useState([]);
  const[genresbyId, setGenresbyId]=useState<any>();
  const[review, setReview]=useState<any>();
  const[ava, setAva]=useState<any>();

  //API Detail Movie
  const Detail  = async () => {
          Promise.all([
              fetch(`http://api.themoviedb.org/3/movie/${moviesId.id}?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US`),
              fetch(`https://api.themoviedb.org/3/movie/${moviesId.id}/credits?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US`),
              fetch(`https://api.themoviedb.org/3/movie/${moviesId.id}/videos?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US`),
              fetch(`https://api.themoviedb.org/3/movie/${moviesId.id}/similar?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US&page=1`),
              fetch(`https://api.themoviedb.org/3/movie/${moviesId.id}/reviews?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US&page=1`)
            ])
              .then(resp => Promise.all( resp.map(r => r.json()) ))
              .then(
                  ([data, cast, trailer,similar,review ])=> {
                      setIsLoaded(true);
                      console.log(data)

                      setMovie(data);   
                      setGenresbyId(data.genres);
                      console.log(cast)
                      setCast(cast.cast); 
                      //console.log(trailer)
                      setTrailer(trailer.results); 
                      setReview(review.results); 
                      console.log(review)
                      const ava=review.results.author_details?.reduce((ava:any,avas:any) => {
                          const {avatar_path,username} = avas
                          ava[avatar_path] =username
                          return ava
                      },[])
                      setAva(review);
                      console.log(ava) 
                      setSimilar(similar.results); 
                      //console.log(similar)
                  },
                  (error)=>{
                      setIsLoaded(true);
                      setError(error);
                  }
              )
             
              window.scrollTo({
                  top: 0,
                  left: 0,
                  behavior: "smooth"
                });
                
  }
  useEffect(() => {
    Detail()

  }, []);

  return {moviesId, isLoaded,cast , movie, error,trailer ,similar,genresbyId,review,ava};

}

