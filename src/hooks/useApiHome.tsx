import React, { useEffect, useState } from 'react'
import apiSettings from '../service/Api';

export type Props = {
    isLoaded: Boolean;
    moviesList:any;
    TvList:any;
    genres:any;
    error: any;
}

export const useApiHome  = (): Props => {
    const [error, setError] = useState<any>();
    const [isLoaded, setIsLoaded] = useState<boolean>(false);
    const [moviesList, setMoviesList] = useState<any>();
    const [TvList, setTvList] = useState<any>();
    const[genres, setGenres]=useState<any>();

    const Hero  = async () => {
            Promise.all([
                fetch (apiSettings.fetchMovielist),
                fetch(apiSettings.fetchGenrelist)
              ])
            .then(resp => Promise.all( resp.map(r => r.json()) ))
            .then(
                ([data, genreData])=> {
                    setIsLoaded(true);
                    console.log(data)             

                    setMoviesList(data.results);   
                    console.log(genreData)
                    const genres=genreData.genres?.reduce((genres:any,gen:any) => {
                        const {id,name} = gen
                        genres[id] =name
                        return genres
                    },[])
                    setGenres(genres);                                            
                },
                (error)=>{
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }

    const ForYou  = async () => {
        Promise.all([
            fetch (apiSettings.fetchMoviePopular),
            fetch(apiSettings.fetchGenrelist)
          ])
        .then(resp => Promise.all( resp.map(r => r.json()) ))
        .then(
            ([data, genreData])=> {
                setIsLoaded(true);
                console.log(data)                    
                setMoviesList(data.results);   
                console.log(genreData)
                const genres=genreData.genres?.reduce((genres:any,gen:any) => {
                    const {id,name} = gen
                    genres[id] =name
                    return genres
                },[])
                setGenres(genres);   
                                     
            },
            (error)=>{
                setIsLoaded(true);
                setError(error);
            }
        )    
    }

    const Trending  = async () => {
        Promise.all([
            fetch (apiSettings.fetchMovieTrending),
            fetch(apiSettings.fetchGenrelist)
          ])
        .then(resp => Promise.all( resp.map(r => r.json()) ))
        .then(
            ([data, genreData])=> {
                setIsLoaded(true);
                console.log(data)                    
                setMoviesList(data.results);   
                console.log(genreData)
                const genres=genreData.genres?.reduce((genres:any,gen:any) => {
                    const {id,name} = gen
                    genres[id] =name
                    return genres
                },[])
                setGenres(genres);   
                                     
            },
            (error)=>{
                setIsLoaded(true);
                setError(error);
            }
        )    
    }

    const TvSeries  = async () => {
        Promise.all([
            fetch (apiSettings.fetchTvlist),
            fetch(apiSettings.fetchGenrelist)
          ])
        .then(resp => Promise.all( resp.map(r => r.json()) ))
        .then(
            ([data, genreData])=> {
                setIsLoaded(true);
                console.log(data)                                    
                setTvList(data.results);   
                console.log(genreData)
                const genres=genreData.genres?.reduce((genres:any,gen:any) => {
                    const {id,name} = gen
                    genres[id] =name
                    return genres
                },[])
                setGenres(genres);   
                                     
            },
            (error)=>{
                setIsLoaded(true);
                setError(error);
            }
        )    
    }

    useEffect(() => {
        Hero()
        ForYou()
        Trending()
        TvSeries()

      }, []);
      
      return { isLoaded, moviesList, genres, error,TvList };
}