import React from 'react'
import { Route, Routes } from 'react-router-dom';
import NotFound from '../layout/NotFound';
import MoviePage from '../page/Detail/MoviePage';
import TvPage from '../page/Detail/TvPage';
import Home from '../page/Home';
import Kids from '../page/Kids';
import Movie from '../page/Movie';
import Tv from '../page/Tv';


const Router = () => {
  return (
    <Routes >
            <Route  path="" element={<Home />}/>
            <Route  path="home" element={<Home />}/>
            <Route  path="movie" element={<Movie />}/>
            <Route  path="movie/:id" element={<Movie />}/>
            <Route  path="movie/:id/page/:num" element={<Movie />}/>
            <Route  path="tv" element={<Tv />}/>
            <Route  path="tv/:id" element={<Tv />}/>
            <Route  path="tv/:id/page/:num" element={<Tv />}/>
            <Route  path="moviepage" element={<MoviePage />}/>
            <Route path="moviepage/:id" element={<MoviePage />} />
            <Route  path="tvshow" element={<TvPage />}/>
            <Route path="tvshow/:id" element={<TvPage />} />
            <Route  path="kids" element={<Kids />}/>
            <Route path="*" element={<NotFound />} />
    </Routes>
  )
}
export default Router;