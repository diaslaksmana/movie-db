import * as React from "react"

function SVGLoading(props:any) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      style={{
        margin: "auto",
        background: "0 0"
      }}
      width="200px"
      height="200px"
      viewBox="0 0 100 100"
      preserveAspectRatio="xMidYMid"
      display="block"
      {...props}
    >
      <circle cx={45} cy={50} fill="#f9ae5c" r={5}>
        <animate
          attributeName="cx"
          repeatCount="indefinite"
          dur="1.2048192771084336s"
          keyTimes="0;0.5;1"
          values="45;55;45"
          begin="-0.6024096385542168s"
        />
      </circle>
      <circle cx={55} cy={50} fill="#e4812f" r={5}>
        <animate
          attributeName="cx"
          repeatCount="indefinite"
          dur="1.2048192771084336s"
          keyTimes="0;0.5;1"
          values="45;55;45"
          begin="0s"
        />
      </circle>
      <circle cx={45} cy={50} fill="#f9ae5c" r={5}>
        <animate
          attributeName="cx"
          repeatCount="indefinite"
          dur="1.2048192771084336s"
          keyTimes="0;0.5;1"
          values="45;55;45"
          begin="-0.6024096385542168s"
        />
        <animate
          attributeName="fill-opacity"
          values="0;0;1;1"
          calcMode="discrete"
          keyTimes="0;0.499;0.5;1"
          dur="1.2048192771084336s"
          repeatCount="indefinite"
        />
      </circle>
    </svg>
  )
}

export default SVGLoading
