// Configuration for TMDB API
// Read more about the API here: https://developers.themoviedb.org/

const API_URL: string  = "https://api.themoviedb.org/3/"
const API_KEY: string | undefined = "45bf6592c14a965b33549f4cc7e6c664"
const options = {
  method: 'GET',
  headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
  }      
};


const SEARCH_BASE_URL: string  = `${API_URL}search/movie?api_key=${API_KEY}&language=en-US&query=,${options}`;
const NOW_PLAYING_BASE_URL: string = `${API_URL}movie/now_playing?api_key=${API_KEY}&language=en-US,${options}`;
const GENRE_MOVIE_BASE_URL: string = `${API_URL}genre/movie/list?api_key=${API_KEY}&language=en-US,${options}`;
const MOVIE_POPULAR_BASE_URL: string = `${API_URL}movie/top_rated?api_key=${API_KEY}&language=en-US,${options}`;
const TV_POPULAR_BASE_URL: string = `${API_URL}tv/top_rated?api_key=${API_KEY}&language=en-US,${options}`;
const KIDS_MOVIE_BASE_URL: string = `${API_URL}discover/tv?api_key=${API_KEY}&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=1&with_genres=10762,${options}`;
const GENRE_TV_BASE_URL: string = `${API_URL}genre/movie/list?api_key=${API_KEY}&language=en-US,${options}`;
const GENRE_MOVIE_TRENDING_BASE_URL: string = `${API_URL}trending/movie/day?api_key=${API_KEY}&language=en-US,${options}`;

const IMAGE_BASE_URL: string = 'http://image.tmdb.org/t/p/'
// Sizes: w300, w780, w1280, original
const BACKDROP_SIZE: string = 'original/'
// w92, w154, w185, w342, w500, w780, original
const POSTER_SIZE: string = "w780/"
// w92, w154, w185, w342, w500, w780, original
const PROFILE_SIZE: string = "w185/"

export {
  SEARCH_BASE_URL,
  NOW_PLAYING_BASE_URL,
  GENRE_MOVIE_BASE_URL,
  MOVIE_POPULAR_BASE_URL,
  GENRE_MOVIE_TRENDING_BASE_URL,
  API_URL,
  API_KEY,
  IMAGE_BASE_URL,
  BACKDROP_SIZE,
  POSTER_SIZE,
  PROFILE_SIZE,
  TV_POPULAR_BASE_URL,
  GENRE_TV_BASE_URL,
  KIDS_MOVIE_BASE_URL
}
