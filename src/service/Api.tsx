import {
    NOW_PLAYING_BASE_URL,
    GENRE_MOVIE_BASE_URL,
    TV_POPULAR_BASE_URL,
    MOVIE_POPULAR_BASE_URL,
    GENRE_TV_BASE_URL,
    GENRE_MOVIE_TRENDING_BASE_URL,
    KIDS_MOVIE_BASE_URL
  } from './Config';
    
   
  const apiSettings = {
   
    fetchMovielist:`${NOW_PLAYING_BASE_URL}`,
    fetchGenrelist:`${GENRE_MOVIE_BASE_URL}`,
    fetchTvlist:`${TV_POPULAR_BASE_URL}`,
    fetchGenreTvlist:`${GENRE_TV_BASE_URL}`,
    fetchMoviePopular:`${MOVIE_POPULAR_BASE_URL}`,
    fetchMovieTrending:`${GENRE_MOVIE_TRENDING_BASE_URL}`,
    fetchKidsMovie:`${KIDS_MOVIE_BASE_URL}`
   
    }

  
  export default apiSettings;