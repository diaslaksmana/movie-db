import React, { useState } from 'react'
import { convertRated } from '../../helper/Helper'
import Imdb from '../../assets/imdb.png'
import { BACKDROP_SIZE, IMAGE_BASE_URL, POSTER_SIZE } from '../../service/Config'
import LoadingPage from '../../layout/loading/LoadingPage'
import { useApiTvDetail } from '../../hooks/useApiTvDetail'
type Props = {}
const imagePerRow = 4;
const TvPage = (props: any) => {
    const{ error, isLoaded,cast,movie,trailer,similar,genresbyId,review,season,seasonNum,setSeasonNum ,episode,showTrailer, setShowTrailer }= useApiTvDetail();
    const [num, setNum] = useState(5);
    const handleMoreImage = () => {
        setNum(num + imagePerRow);
      };
    
    if (error) {
        return <div>Error</div>;
    } else if (!isLoaded) {
        return <LoadingPage/>;
    } else {
  return (
    <section id='detail'>
        <div className='wrapper'>
            <div className='thumbnail'>
                <img src={`${IMAGE_BASE_URL}${BACKDROP_SIZE}${movie?.backdrop_path}`}/>
            </div>
            <div className='overview mx-auto  mx-aut py-4 px-4 sm:px-6 lg:px-8'>
            <div className="block sm:flex sm:flex-row gap-10">
                <div className='basis-1/4 cover'>
                <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}${movie?.poster_path}`}/>
                </div>
                <div className='basis-3/4 desc'>
                    <h1 className='text-2xl sm:text-5xl font-bold text-white'>
                    {movie?.name}
                    </h1>
                    <ul className='flex flex-nowrap'>
                        <li>
                            <div className='rating my-3'>
                                <div className='rate'>
                                {convertRated(movie?.vote_average)}
                                </div>
                                <img src={Imdb}/>
                            </div>
                           
                        </li>
                        <li>
                            <p className='text-white my-4 text-md sm:text-lg'>
                                <span className='font-bold'>Release</span>: {movie.first_air_date}
                             </p>
                        </li>
                    </ul>
                    <div className='genre text-white my-3'>
                        {genresbyId?.map((genre:any) => (
                            <span key={genre.id} className="text-xs sm:text-md"> {genre.name}</span> 
                        ))}
                    </div>
                    <p className='text-white my-3 text-sm sm:text-1xl'>
                        {movie?.overview}
                    </p>
                    <h5 className='text-lg text-white'>Cast</h5>
                    <ul className='cast'>
                        {cast?.slice(0, num)?.map((cast:any,index:any) => (
                        <li key={index}>                                              
                            {cast?.profile_path !== null ? (
                            <img
                                src={
                                    "https://image.tmdb.org/t/p/w185/" +
                                        cast?.profile_path
                                    }
                                    alt={cast.name}
                                />
                                ) : (
                                <img
                                    src=''
                                    alt={cast.name}
                                    />
                            )}
                            <p className='text-white text-xs'>{cast.name}</p>
                        </li>
                        ))}
                        <li>
                            {num < cast?.length && (
                                <button
                                className="load-more font-bold"
                                onClick={ handleMoreImage}
                                >
                                See More
                                </button>
                            )}
                        </li>
                    </ul>
                    <div className=' my-5'>
                        <h5 className='text-white text-lg'>Trailer</h5>
                        {trailer?.slice(0,1).map((trailer:any) => (
                        <div key={trailer.id}>
                            <iframe
                                width="100%"
                                height="480"
                                src={"https://www.youtube.com/embed/" + trailer?.key}
                                frameBorder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                allowFullScreen
                                title="YouTube video player"
                            />        
                        </div>
                        ))} 
                    </div>
                </div>
            </div>
            </div>
        </div>
    </section>
  )
    }
}

export default TvPage