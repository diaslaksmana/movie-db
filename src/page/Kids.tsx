import React, { useState } from 'react'
import { Link } from 'react-router-dom';
import { convertRated } from '../helper/Helper';
import { useApiKidsMovie } from '../hooks/useApiKidsMovie';
import { IMAGE_BASE_URL, POSTER_SIZE } from '../service/Config';

type Props = {}

const Kids = (props: any) => {
    const {isLoaded, moviesList, genres, error ,pages} = useApiKidsMovie();
    const [num, setNum] = useState(1);
  return (
    <section id='kids-movie'>
        <div className=' mx-3  py-4 sm:mx-auto sm:px-6 lg:px-8'>
            <h1 className='text-white text-2xl mt-20'>Kids Movie</h1>
            <div className="grid grid-cols-2 sm:grid-cols-5 gap-4  my-10">
                        { moviesList?.map((movie:any,index:any) => (
                          <div className='card' key={index}>
                            <figure className="snip1527">
                              <div className="image"><img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ movie.poster_path} className='w-[100%] h-[350px] object-cover object-center rounded-md'/></div>
                              <figcaption>
                              <div className="date"><span className="day">{convertRated(movie.vote_average)}</span></div>
                                <h3>{movie.name}</h3>
                                <p>
                                  {movie.overview.slice(0, 80) + `...`}
                                </p>
                              </figcaption>
                              <Link  to={`/tvshow/${movie.id}`}></Link>
                            </figure>
                          </div>   
                        ))}              
            </div>
            {/* Pagination */}
            <div className='pagination my-14'>
                  <ul className='flex flex-row justify-center items-center pl-0'>
                    <li>
                      <button 
                        className='next'
                        disabled={  pages?.page == 1}
                        onClick={() => setNum(num - 1)}
                      >
                        Prev
                      </button>
                    </li>
                    <li>
                      <div className='current-page'> 
                        <span className='font-bold'>{pages?.page}</span>
                      </div>
                    </li>
                    <li>
                      <button 
                        className='prev'
                        disabled={pages?.page === pages?.total_pages}
                        onClick={() => setNum(num + 1)}
                      >
                        Next
                      </button>
                    </li>
                  </ul>
                </div>
        </div>
    </section>
  )
}

export default Kids