import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { convertRated } from '../helper/Helper';
import LoadingPage from '../layout/loading/LoadingPage';
import LoadingSection from '../layout/loading/LoadingSection';
import { IMAGE_BASE_URL, POSTER_SIZE } from '../service/Config';

type Props = {}

const Tv = (props: Props) => {
    const params = useParams();
    const[error, setError]= useState<any>();
    const[isLoaded, setIsLoaded]=useState(false);
    const[tvShow, setTvShow]=useState<any>();
    const[genres, setGenres]=useState<any>();
    const [pages, setPages] = useState<any>();
    const [num, setNum] = useState(1);
  
    
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: "smooth"
    });
    const options = {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        }      
      };
        useEffect (() =>{
                Promise.all([
                    fetch(`https://api.themoviedb.org/3/discover/tv?api_key=06b60557a575f53871415fab9e43d082&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=${num}&with_genres=${params.id}`,options ),                  
                    fetch(`https://api.themoviedb.org/3/genre/tv/list?api_key=45bf6592c14a965b33549f4cc7e6c664&language=en-US`,options),
                  ])
                  .then(resp => Promise.all( resp.map(r => r.json()) ))
                .then(
                    ([data, genreData])=> {
                        setIsLoaded(true);
                        console.log(data)
                        setTvShow(data.results);
                        setPages(data);                 
                        setGenres(genreData.genres);
                        console.log(genreData)
                        setIsLoaded(true);
                        //setValue(params.id);
                     
                    },
                    (error)=>{
                        setIsLoaded(true);
                        setError(error);
                    }
                )
        },[num,params.id])
      if (error) {
        return <div>Error </div>;
      } else if (!isLoaded) {
        return <LoadingPage/>;
      } else {
      return (
        <section id='movie'>
          <div className='mx-auto  mx-aut py-4 sm:px-6 lg:px-8 flex flex-row'>
            <div className='aside basis-1/6 sticky '>
              <div className='filter'>
                <h1 className='text-2xl font-bold text-white mb-5'>Tv Genre</h1>
                <ul className='list-genres'>
                  {genres?.map((genre:any) => (   
                  <li className='mb-2' key={genre.id}>
                    <NavLink  to={`/tv/${genre.id}`} onClick={() => setNum(1)} className='text-white hover:text-[#575A4E]'> 
                      {genre.name}
                    </NavLink>
                  </li>
                  ))} 
                </ul>
              </div>
            </div>
            <div className='content basis-5/6'>
              <h1 className='text-2xl font-bold text-white mb-5'>Result</h1>
              
              <div className='card-movie'>          
                  {!isLoaded ? (<LoadingSection/>) :
                    (
                      <div className="grid grid-cols-2 sm:grid-cols-5 gap-4  my-10">
                        { tvShow?.map((movie:any,index:any) => (
                          <div className='card' key={index}>
                            <figure className="snip1527">
                              <div className="image"><img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ movie.poster_path} className='w-[100%] h-[350px] object-cover object-center rounded-md'/></div>
                              <figcaption>
                              <div className="date"><span className="day">{convertRated(movie.vote_average)}</span></div>
                                <h3>{movie.name}</h3>
                                <p>
                                  {movie.overview.slice(0, 80) + `...`}
                                </p>
                              </figcaption>
                              <Link  to={`/tvshow/${movie.id}`}></Link>
                            </figure>
                          </div>   
                        ))}              
                      </div>
                    )
                  }
                {/* Pagination */}
                <div className='pagination my-14'>
                  <ul className='flex flex-row justify-center items-center pl-0'>
                    <li>
                      <button 
                        className='next'
                        disabled={  pages?.page == 1}
                        onClick={() => setNum(num - 1)}
                      >
                        Prev
                      </button>
                    </li>
                    <li>
                      <div className='current-page'> 
                        <span className='font-bold'>{pages?.page}</span>
                      </div>
                    </li>
                    <li>
                      <button 
                        className='prev'
                        disabled={pages?.page === pages?.total_pages}
                        onClick={() => setNum(num + 1)}
                      >
                        Next
                      </button>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
      
        </section>
      )
    }
  }

export default Tv