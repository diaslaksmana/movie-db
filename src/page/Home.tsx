import React from 'react'
import Hero from '../layout/Hero'
import ForYour from '../layout/listing/ForYour'
import Trending from '../layout/listing/Trending'
import TvShow from '../layout/listing/TvShow'

type Props = {}

const Home = (props: Props) => {
  return (
    <main>
      <Hero/>
      <ForYour/>
      <Trending/>
      <TvShow/>
    </main>
  )
}

export default Home