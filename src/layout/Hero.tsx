import React from 'react'

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import Hero1 from '../assets/hero1.jpg'
import Hero2 from '../assets/hero2.jpg'
import Hero3 from '../assets/hero3.jpg'
import Imdb from '../assets/imdb.png'
// import required modules
import {Autoplay, Pagination, Navigation } from "swiper";
import { useApiHome } from '../hooks/useApiHome';
import { BACKDROP_SIZE, IMAGE_BASE_URL } from '../service/Config';
import { convertRated } from '../helper/Helper';
import { Link } from 'react-router-dom';
import LoadingSection from './loading/LoadingSection';

const Hero = (props: any) => {
    const {error,isLoaded,moviesList,genres} = useApiHome();
    if (error) {
        return <div>Error </div>;
    } else if (!isLoaded) {
        return <LoadingSection/>;
    } else {
        return (
            <section className='wrapper'>
                <div className='hero'>
                    <Swiper
                        pagination={{
                        type: "progressbar",
                        }}
                        loop={true}
                        autoplay={{
                            delay: 2500,
                            disableOnInteraction: false,
                        }}
                        navigation={false}
                        modules={[Autoplay,Pagination, Navigation]}
                        className="mySwiper"
                    >
                        {moviesList?.map((movie:any) => (
                        <SwiperSlide key={movie.id}>
                            <Link  to={`/moviepage/${movie.id}`}>
                            <div className='thumbnail'>
                                <div className='cover'>                                    
                                        <img src={`${IMAGE_BASE_URL}${BACKDROP_SIZE}`+ movie.backdrop_path} className='w-[100%] h-[100vh] object-cover object-bottom'/> 
                                </div>
                                <div className='overview mx-3  py-4 sm:mx-auto sm:px-6 lg:px-8'>
                                    <h1 className='title text-white text-2xl sm:text-5xl font-bold'>
                                    {movie.title} 
                                    </h1>
                                    <p className='text-white my-4 text-md sm:text-lg'>
                                        <span className='font-bold'>Release</span>: {movie.release_date}
                                    </p>
                                    <p className='desc text-white my-4 text-sm'>
                                    {movie.overview}
                                    </p>
                                    <ul>
                                        <li>
                                            <div className='rate'>
                                            {convertRated(movie.vote_average)}
                                            </div>
                                            <img src={Imdb}/>
                                        </li>                                       
                                        <li>
                                            <div className='genre text-white '>
                                                {movie?.genre_ids.slice(0,3).map((id:any) => (
                                                    <span key={id} className="text-xs sm:text-lg"> {genres[id]}</span> 
                                                ))} 
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            </Link> 
                        </SwiperSlide>
                        ))}

                    </Swiper>
                </div>
            </section>
        )
    }
}

export default Hero