import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { NavLink } from 'react-router-dom'
import Logo from '../assets/logo.png'
import Search from '../assets/svg/Search'
import { IMAGE_BASE_URL, POSTER_SIZE } from '../service/Config'

  
const Header = (props: any) => {
    const [headerClassName, setHeaderClassName] = useState('');
    const [data, setData] = useState<any>([]);
    const [value, setValue] = useState("");
    const options = {
      method: 'GET',
      headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        }      
    };
      
      let num =1
      let apiSearch=`https://api.themoviedb.org/3/search/multi?api_key=06b60557a575f53871415fab9e43d082&language=en-US&query=${value}&page=${num}&include_adult=true`
      
      const onChange = (event:any) => {
        setValue(event.target.value);
      };
        useEffect(() => {  
            fetch(apiSearch)
            .then(res => res.json())
            .then(
                (data) => {
                    setData(data.results);                      
                    console.log(data);                  
                }
                
            )
            window.onscroll = () => handleScroll(headerClassName);
        }, [headerClassName,apiSearch])
        const handleScroll = (headerClassName:any) => {
            if (window.scrollY >= 100) {
                document.body.classList.add("scrolled");
            } else {
                document.body.classList.remove("scrolled");
            }
        }
     
   
  
  return (
    <header className={`fixed z-[9999] w-[100%] top-0 header `}>
      <nav>
        <div className="mx-auto  mx-aut py-4 px-4 sm:px-6 lg:px-8">
            <div className="relative flex  items-center justify-between">
         
            <div className="flex flex-1 items-center justify-between sm:items-stretch sm:justify-start">
                <div className="flex flex-shrink-0 items-center">
                <img className="block h-8 w-auto lg:hidden" src={Logo} alt="Your Company"/>
                <img className="hidden h-8 w-auto lg:block" src={Logo} alt="Your Company"/>
                </div>
                <div className="hidden sm:ml-6 sm:block">
                <div className="flex space-x-4 nav-menu">
                    {/* <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" --> */}
                  
                    <NavLink to="/" className=" text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-lg font-medium font-Nunito" aria-current="page">
                        Home
                    </NavLink>

                    <NavLink to="/movie" className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-lg font-medium">
                        Movie
                    </NavLink>

                    <NavLink to="/tv" className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-lg font-medium">
                        Tv Series
                    </NavLink>
                    <NavLink to="/kids" className="text-yellow hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-lg font-extrabold">
                        Kids
                    </NavLink>

                </div>
                </div>
            </div>
            <div className="absolute inset-y-0 right-0 flex items-center pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0">
            
                <div className='searchbar flex relative justify-start items-center'>
                    <Search/>
                    <input className=' h-9 ' placeholder='Search Movie' value={value} onChange={onChange}/>
                    <div className='suggestion'>
                    {data?.filter((item:any) => {
                                    const titleMovie = item?.title?.toLowerCase();
                                    return (
                                        titleMovie
                                        
                                    );
                                    }).map((item:any) => (                                    
                                    <div className="suggestion-link " key={item.id}> 
                                        
                                        <Link  to={`/moviepage/${item.id}`} className='flex'  target='_blank'>
                                        <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ item.poster_path} className='w-[50px] h-[70px] mr-5 object-cover object-center rounded-md'/> {item.title} -  {item.media_type}
                                        </Link>                                 
                                    </div>
                                ))}

                                {data?.filter((item:any) => {
                                    const keyName = item?.name?.toLowerCase();
                                    return (
                                        keyName  
                                        
                                    );
                                    }).map((item:any) => (
                                        
                                    <div className="suggestion-link " key={item.id}>   
                                        <Link  to={`/tvshow/${item.id}`} className='flex' target='_blank'>
                                        <img src={`${IMAGE_BASE_URL}${POSTER_SIZE}`+ item.poster_path} className='w-[50px] h-[70px] mr-5 object-cover object-center rounded-md'/>{item.name} - {item.media_type}
                                        </Link>                                 
                                    </div>
                                ))}             
                    </div>
                </div>

                
                
            </div>
            </div>
        </div>

        {/* <!-- Mobile menu, show/hide based on menu state. --> */}
        <div className="sm:hidden bg-dark" id="mobile-menu">
            <div className="space-y-1 px-2 pt-2 pb-3 flex justify-center items-center">
            {/* <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" --> */}
            <NavLink to="/" className=" text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-lg font-medium font-Nunito" aria-current="page">
                        Home
            </NavLink>

            <NavLink to="/movie" className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-lg font-medium">
                        Movie
            </NavLink>
            <NavLink to="/movie" className="text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-lg font-medium">
                        Tv Series
                    </NavLink>
                    <NavLink to="/movie" className="text-yellow hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-lg font-extrabold">
                        Kids
                    </NavLink>
            </div>
        </div>
        </nav>
    </header>
  )
}

export default Header