import React from 'react'
import SVGLoading from '../../assets/svg/loading'

type Props = {}

const LoadingPage = (props: Props) => {
  return (
    <section id='loading'>
        <SVGLoading/>
    </section>
  )
}

export default LoadingPage