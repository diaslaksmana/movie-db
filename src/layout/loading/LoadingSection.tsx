import React from 'react'
import SVGLoading from '../../assets/svg/loading'

type Props = {}

const LoadingSection = (props: Props) => {
  return (
    <section id='loading_section'>
        <SVGLoading/>
    </section>
  )
}
export default LoadingSection