import React from 'react'
import { NavLink } from 'react-router-dom'
import Notfound from '../assets/svg/Notfound'

type Props = {}

const NotFound = (props: Props) => {
  return (
    <section className='flex justify-center items-center h-[100vh] not-found'>
        <Notfound/>
        <h1 className='text-white text-5xl mb-3'>Page Not Found</h1>
        <NavLink to="/" className=" text-gray-300 hover:bg-gray-700 hover:text-white px-3 py-2 rounded-md text-lg font-medium font-Nunito" aria-current="page">
            Back to Home
        </NavLink>
    </section>
  )
}

export default NotFound