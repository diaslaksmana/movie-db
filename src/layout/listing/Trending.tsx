import React from 'react'
import Hero1 from '../../assets/hero1.jpg'
import Hero2 from '../../assets/hero2.jpg'
import Hero3 from '../../assets/hero3.jpg'
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";


// import required modules
import { Autoplay } from "swiper";
import { useApiHome } from '../../hooks/useApiHome';
import { BACKDROP_SIZE, IMAGE_BASE_URL } from '../../service/Config';
import { convertRated } from '../../helper/Helper';
import { Link } from 'react-router-dom';
type Props = {}

const Trending = (props: Props) => {
    const {error,isLoaded,moviesList,genres} = useApiHome();
    if (error) {
        return <div>Error </div>;
    } else if (!isLoaded) {
        return <div>Loading</div>;
    } else {
  return (
    <section className='wrapper bg-dark'>
    <div className='mx-auto  mx-aut py-4  px-4 sm:px-6 lg:px-8'>
    <h1 className='text-4xl font-bold mb-3 text-white'>Trending</h1>
    <Swiper
       slidesPerView={1}
       spaceBetween={50}
       pagination={{
       clickable: true,
       }}
       breakpoints={{
           640: {
             slidesPerView: 3,
           },
           768: {
             slidesPerView: 4,
           },
           1024: {
             slidesPerView: 5.5,
           },
         }}
         loop={true}
         autoplay={{
            delay: 3500,
            disableOnInteraction: false,
        }}
        modules={[Autoplay]}
        className="mySwiper"
    >
       {moviesList?.map((movie:any) => (
            <SwiperSlide  key={movie.id}>
                <div className='card'>
                    <figure className="snip1527">
                        <div className="image"><img src={`${IMAGE_BASE_URL}${BACKDROP_SIZE}`+ movie.poster_path} className='w-[100%] h-[350px] object-cover object-center rounded-md'/></div>
                        <figcaption>
                            <div className="date"><span className="day">{convertRated(movie.vote_average)}</span></div>
                             <h3> {movie.title} </h3>
                            <p>
                                {movie.overview.slice(0, 100) + `...`}
                            </p>
                        </figcaption>
                        <Link  to={`/moviepage/${movie.id}`}></Link>
                    </figure>
                </div>
            </SwiperSlide>
        ))}
        

    </Swiper>
    </div>
</section>
  )
    }
}

export default Trending