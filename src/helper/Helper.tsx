// Convert time to hours and minutes
export const calcTime = (time:any) => {
    const hours = Math.floor(time / 60)
    const mins = time % 60
    return `${hours}h ${mins}m`
  }
  // Convert a number to money formatting
  export const convertMoney = (money:any) => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      minimumFractionDigits: 0
    })
    return formatter.format(money)
  }
  export const convertRated = (rated:any) =>{
    const rate= rated.toFixed(1);
    return `${rate}`
  }
  export const isPersistedState = (stateName:any) => {
    const sessionState = sessionStorage.getItem(stateName)
    return sessionState && JSON.parse(sessionState)
  }
  